import java.util.Random;

public class Threads implements Runnable {

    private String name;
    private int number;
    private int sleep;
    private int rand;

    public Threads(String name, int number, int sleep) {

        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(500);
    }

    public void run() {
        System.out.println("\nShhh. Be vewy vewy quiet, I’m hunting wabbits : Rabbit =" + name + " Distance = "
        + number + " It’s Hunting Season = " + sleep + " 0-250=Hit 251-500=Miss = " + rand + "\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is getting away hurry. ");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n" + name + " got away.\n”);
    }
}