import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThreads {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(5);

        Threads t1 = new Threads(“Bugs”, 40, 400);
        Threads t2 = new Threads(“Lola”, 20, 200);
        Threads t3 = new Threads(“Peter”, 30, 300);
        Threads t4 = new Threads(“Thumper”, 50, 500);
        Threads t5 = new Threads(“Snowball”, 10, 100);

        myService.execute(t1);
        myService.execute(t2);
        myService.execute(t3);
        myService.execute(t4);
        myService.execute(t5);

        myService.shutdown();
    }
}